class php {
  # Make sure php5 is present
  package {'php5':  }

  package {'php-pear':
    ensure => present,
    require => Package['php5'],
  }

  # List php enhancers modules
  $php_enhancers = [ 'php5-intl', 'php5-mysql', 'php5-fpm' ]
  # Make sure the php enhancers are installed
  package { $php_enhancers:
    ensure  => installed,
    require => Package['php5'],
    notify  => Service['apache2'],
  }

  # Create a php config file that meets the symfony2 requirements
  file {'symfony2-php-conf':
    ensure  => present,
    path    => '/etc/php5/apache2/conf.d/symfony2-php-conf.ini',
    owner   => 'root',
    group   => 'root',
    content => template('php/symfony2-php-conf.erb'),
    require => Package['php5'],
  }
}